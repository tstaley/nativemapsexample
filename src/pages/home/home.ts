import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, LoadingController } from 'ionic-angular';

declare var plugin: any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('mapdiv') mapdiv: ElementRef;

  constructor(public navCtrl: NavController,  public platform: Platform, public loadingController: LoadingController) {
    
  }

  ionViewDidLoad(){
    
    this.platform.ready().then(() => {
      this.loadMap();
    });
  }

  loadMap() {

    // this.loadingProvider.show();
    // let element: HTMLElement = document.getElementById('map');
    let point = new plugin.google.maps.LatLng(38.331161,-93.19164);

    let options = {
      'mapType': 'TERAIN',
      'controls': {
        'compass': true,
        'myLocationButton': true
      },
      'gestures': {
        'scroll': true,
        'tilt': false,
        'rotate': true,
        'zoom': true
      },
      'camera': {
        'latLng': point,
        'zoom': 15
      }
    };
    let map = new plugin.google.maps.Map.getMap(this.mapdiv.nativeElement, options);
    let loading = this.loadingController.create({spinner: 'circles', content: 'Please wait...'});
    map.one(plugin.google.maps.event.MAP_READY, () => {
      console.log('Map is ready!');

      map.on(plugin.google.maps.event.MAP_CLICK, function(latLng) {
        
        loading.present();
        map.addMarker({
          position: latLng,
          title: latLng,
          animation: plugin.google.maps.Animation.DROP
        });
        loading.dismiss();
      });
    });
  };
}
